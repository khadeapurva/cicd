#!/usr/bin/env groovy
package com.helper

class sampleClass{

    String name
    Integer age

    def increaseAge(Integer years){
        this.age += years
    }
}
